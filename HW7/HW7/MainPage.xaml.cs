﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Net.Http;
using Plugin.Connectivity;
using HW7.Models;
using ModernHttpClient;
using Newtonsoft.Json;
using FormsToolkit;

namespace HW7
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            MessagingService.Current.Subscribe<bool>("connectivityChanged", (args, connected) =>
            {
                if (connected)
                    DisplayAlert("Internet connection found.", "Wait for the application data to update.", "OK");
                else
                    DisplayAlert("Internet connection lost.", "Application data may not be up to date. Connect to a working network.", "OK");
            });
        }

        async void Entry_Completed(object sender, EventArgs e)
        {
            string text = ((Entry)sender).Text; //cast sender to access the properties of the Entry

                    // HttpClient client = new HttpClient();
                    var client = new HttpClient(new NativeMessageHandler());

                    var uri = new Uri(
                    string.Format(
                        $"https://owlbot.info/api/v2/dictionary/" +
                        $"{text}" + $"?format=api"));

                    var request = new HttpRequestMessage();
                    request.Method = HttpMethod.Get;
                    request.RequestUri = uri;
                    request.Headers.Add("Application", "application / json");

                    HttpResponseMessage response = await client.SendAsync(request);
                    Item itemInfo = null;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = await response.Content.ReadAsStringAsync();
                        itemInfo = Item.FromJson(content);
                        InfoType.Text = $"The type is: {itemInfo.Type}";
                        InfoDef.Text = $"The definition is: {itemInfo.Definition}";
                        InfoEx.Text = $"The example is: {itemInfo.Example}";
                    }
        }
    }
}
