﻿using FormsToolkit;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace HW7
{
	public partial class App : Application
	{
		public App ()
		{
			InitializeComponent();

			MainPage = new HW7.MainPage();
		}

		protected override void OnStart ()
		{
            CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
            {
                MessagingService.Current.SendMessage("connectivityChanged", args.IsConnected);
            };// Handle when your app starts
        }

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
